### 猎熊座演示平台

通过 Jenkins 进行自动化部署，代码 push 到 master 分支后自动部署到`192.168.199.169`的 apps，html 文件夹中，expression-platform 这个文件夹不要删除，自动化部署是直接写到这个文件夹中的。
网站的静态及资源文件都在 oss 的 bh-frontend 文件夹中的 expression-platform 目录。


等待后续运维重构好公司网络，这个部署的地址要改成公司内网的演示机器环境，然后打通外网，能够通过外网进行访问。
